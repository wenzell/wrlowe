//Wenzel Roscoe Lowe
window.overlay = function(canvasName, background,foreground){
    filter = foreground;
    img = background;

    stage = new createjs.Stage(canvasName);
    console.log(canvasName)
    console.log(img)
    createjs.Touch.enable(stage);
    var white = new createjs.Graphics().beginFill("#000").drawRect(0,0,800,800);
    var loadWhite = new createjs.Bitmap(white);
    stage.addChild(loadWhite)
    console.log('step1')
    initFrame(filter)
    initImage(img)

    function initImage(imgsrc){
      userPic = stage.getChildByName("userPic")
      stage.removeChild(userPic)
      var image = new Image();
      image.src = imgsrc;
      image.onload = handleDraggable;
      console.log('step2')
    }

    function handleDraggable(event){
      var image = event.target;
      var bitmap = new createjs.Bitmap(image);
      bitmap.scaleX = 1
      bitmap.scaleY = 1
      var dragger = new createjs.Container();
      dragger.x = dragger.y = 100;
      dragger.name = "userPic"
      dragger.addChild(bitmap);
      stage.addChild(dragger);
      console.log('stage3')

      dragger.on('mousedown', function(evt){
          var posX = evt.stageX;
          var posY = evt.stageY;
          this.offset = {x: this.x - posX, y: this.y-posY};
      });

      dragger.on("pressmove",function(evt) {
          var posX = evt.stageX;
          var posY = evt.stageY;
          evt.currentTarget.x = posX + this.offset.x;
          evt.currentTarget.y = posY + this.offset.y;
          stage.removeChild(bitmap)
          stage.update();
      });

      dragger.on("pressup",function(evt){
          var frame = stage.getChildByName("frame")
          stage.removeChild(frame)
          stage.update()
          var filter = new Image();
          filter.src = window.currentFilter
          filter.onload = initFrame;
      })

      stage.update()
    }

    function initFrame(event) {
      var image = event;
      var loadedImage = new createjs.Bitmap(image);
      loadedImage.name = "frame"
      loadedImage.scaleX = 1
      loadedImage.scaleY = 1
      loadedImage.x = 0
      loadedImage.y = 0 

      stage.addChild(loadedImage)
      stage.update()
    }

    // We will add functions to our library here !

  }

