
function uploadFile(){
  var photoElm = document.querySelector('#photo')
  var file = document.querySelector('input[type="file"]').files[0];
  var canvas = document.getElementById('canvas');
  var ctx = canvas.getContext('2d');
  var reader = new FileReader();

  reader.onload = function() {
    photoElm.src = reader.result;

    var overlay = document.getElementById("overlay");
    var photo = document.getElementById("photo");

    if (photo.height < photo.width){
      ctx.drawImage(photo, 0,0,photo.width,photo.height,0, 0,1000,754);
      ctx.drawImage(overlay, 0, 0,1000,1000)
    }
    else if ((photo.height == photo.width) || (photo.width/photo.height < 1.15 && photo.width/photo.height >= 1))   {
      ctx.drawImage(photo, 0, 0,1000,1000);
      ctx.drawImage(overlay, 0, 0,1000,1000)}
    else if (photo.height > photo.width) {
      ctx.drawImage(photo,0,0,photo.width,photo.height,(1000-(photo.width/(photo.height/754)))/2,0,(photo.width/(photo.height/754)),754)
      ctx.drawImage(overlay, 0, 0,1000,1000)
    }

      var dataURL = canvas.toDataURL("image/jpeg");

      document.getElementById('toSave').src = dataURL;
  
  }

  if(file){
    reader.readAsDataURL(file);
  }else {
    photoElm.src = ""
  }
}