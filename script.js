//

hasLoaded = false
Home = {
	template:
	`
	<div>
		<div id ="header">
		</div>
		<div id ="sitewrap">
		<div id="main">
		<div id="center">
		<div id="faceimage">
		  <img src="facecircle.png" id="face">
		  </div>

		  <div id="bigtext">
		    <h1 id="first">Hi, my name is Wenzel Lowe.</h1>
		    <hr id="homer">
		    <h1 id="second">I'm a developer and designer.</h1>
		  </div>
		  <div class="words">
		    <p>
		    I like to develop sites and applications (React and Vue!), design UI, use almost all of Adobe CC, film and edit videos, ease-out motion graphics, take photos, be the go-to IT guy, use social media, play the organ, make music, and <a href="http://incmedia.org/inc" style="font-weight:400;color:black">praise</a>. Here's my <a style="font-weight:400;color:rgb(156, 17, 17)" href="/LoweWenzelResume.pdf" target="_blank">resume</a>!
		    </p>
		    <p>
		    Make sure to check out my <a target="_blank" href="http://linkedin.com/in/lowewenzel" style="color:#0077b5"><strong>LinkedIn</strong></a>. And my <a target="_blank" href="http://github.com/lowewenzel" style="color:grey"><strong>Github!</strong></a> Maybe even my social media like <a target="_blank" href="http://instagram.com/lenzelwowe" style="color:#d63a68"><strong>Instagram</strong></a>, <a target="_blank" href="http://facebook.com/lenzelwowe" style="color:#3b5998"><strong>Facebook</strong></a>, or <a target="_blank" href="http://twitter.com/lenzelwowe" style="color:#55acee"><strong>Twitter</strong></a>. Or you could just <a target="_blank" href="mailto:lowewenzel@gmail.com" style="color:#ca2828"><strong>email me</strong></a>.
		    </p>
		  </div>
		</div>
		<router-link to="/portfolio">
		<div id="rightarrow" >
			<p>portfolio</p>
			<i class="material-icons">chevron_right</i>
		</div>
		</router-link>
		</div>
		</div>
	`,
	data: function(){
		return{
		}
	},
	methods:{
	},
	created: function(){
	},
	mounted: function(){
	if(hasLoaded){
		window.location.reload()
	}
  	window.onload = function(){
    var words = document.querySelector('.words')
    var first = document.querySelector('#first')
    var second = document.querySelector('#second')
	var third = document.querySelector('#rightarrow p')
	var homer = document.querySelector('#homer')

    words.classList.add('move');
    first.classList.add('move');
    second.classList.add('move');
	third.classList.add('move');
	homer.classList.add('move');
    document.querySelector('#face').classList.add('move')
    hasLoaded=true;
  	}
	}
}

Portfolio = {
	template:
	`
	<div>
		<div id="cool-container">
			<div id="graphics">
			<router-link to="/portfolio/graphics">

			<video autoplay="autoplay" loop="loop" muted="">
				<source src="media/coffee.webm" type="video/webm" />
		 	</video>
			<span>GRPHCS<br>UI/UX</span>
			</router-link>
			</div>

			<div id="web">
			<router-link to="/portfolio/webdev">
			<video autoplay="autoplay" loop="loop" muted="">
				<source src="media/coding.webm" type="video/webm" />
		 	</video>
			<span>WEBDEV<br>ENG.</span>
			</router-link>
			</div>

			<div id="photo">
			<router-link to="/portfolio/gallery">
			<video autoplay="autoplay" loop="loop" muted="">
				<source src="media/videos.webm" type="video/webm" />
		 	</video>
			<span>PHOTO<br>VIDEO</span>
			</router-link>
			</div>
		</div>

	</div>
	`,
	mounted: function(){
		document.querySelector("body").style.height = '100%';
	}
}

GraphicsGallery = {
	template:
	`
	<div>
		<div id="cooler-container">
			<div id="proto">
			<router-link to="/portfolio/graphics/prototypes">
			<img src="media/card.gif"/>
			<span>PROTOTYPES<br>MOCKUPS</span>
			</router-link>
			</div>

			<div id="gfx">
			<router-link to="/portfolio/graphics/logos">
			<img src="media/shotinthedark.jpg"/>
			<span>LOGOS<br>GRAPHICS</span>
			</router-link>
			</div>
		</div>

	</div>
	`,
	mounted: function(){
		document.querySelector("body").style.height = '100%';
	}
}

GraphicsPage = {
	template:
	`
	<div>
		<h4 class="portHeader">GRAPHICS<br>LOGOS</h4>
		<div class="grid-container">
			<div v-for="photo in grafix" class="portImages">
				<div class="boxImgCont">
					<a :href="photo.url" data-lightbox="graphics" :data-title="photo.title" >
					<img :src="photo.url" :class="{isHorPic : ([0,3,8].indexOf(photo.id) > -1)}">
					</a>
				</div>
			</div>
		</div>
	</div>
	`,
	data: function(){return{
		grafix: [
			{url: "media/shotinthedark.jpg", title:"80s style graphic", id:0},
			{url: "media/unitygames.jpg", title:"Custom \"Parody\" Shirt Designs",id:1},
			{url: "media/incmusic.jpg", title:"INCMusic Collab Poster",id:2},
			{url: "media/NeighborhoodOutreach.jpg", title:"Neighborhood Outreach logo",id:3},
			{url: "media/graphics1.jpg", title:"Northwest Coffee Promo",id:4},  
			{url: "media/graphics2.jpg", title:"Binhi Solid As One Promo",id:5},
			{url: "media/graphics3.jpg", title:"Northwest Cooking Promo",id:6},
			{url: "media/dance.jpg", title:"Dance Workshop Promo", id: 7},
			{url: "media/egames.jpg", title:"E-Games Tournament Logo", id: 8},
			{url: "media/nwc.jpg", title:"Logo", id: 9},
		]
	}}
}

PrototypesPage = {
	template:
	`
	<div>
		<h4 class="portHeader" style="margin-bottom:10px;">PROTOTYPES<br>MOCKUPS</h4>
		<h2 class="portSub">with fully functional live links</h2>
		<div class="grid-container">
			<div v-for="photo in protos" class="portImages">
				<div class="boxImgCont">
					<a :href="photo.url" data-lightbox="graphics" :data-title="photo.title" >
					<img :src="photo.url" :class="{isHorPic : ([3].indexOf(photo.id) > -1)}">
					</a>
				</div>
			</div>
		</div>
	</div>
	`,
	data: function(){return{
		protos: [
			{url: "media/LMCApp.jpg", title:"Los Medanos College Student App Mockups", id:0},
			{url: "media/fudboy.gif", title:"#DailyUI Challenge #001: Login Screen <a target=\"_blank\" href=\"https://framer.cloud/UPxnC\">https://framer.cloud/UPxnC</a>", id:1},
			{url: "media/card.gif", title:"#DailyUI Challenge #002: Credit Card Checkout <a target=\"_blank\" href=\"https://framer.cloud/DdwhW\">https://framer.cloud/DdwhW</a>", id:2},
			{url: "media/shuttlepad.gif", title:"#DailyUI Challenge #003: Landing Page <a target=\"_blank\" href=\"https://framer.cloud/FhCAN\">https://framer.cloud/FhCAN</a>", id:3}
		],
	}}
}

WebdevPage = {
	template:
	`
	<div id="videos-page">
		<h4 class="portHeader"id="photo-video">WEBDEV<br>&<br>ENGINEERING</h4>
		<div v-for="(video, vIndex) in yVideos">
			<div class="video-left vd" v-if="vIndex%2 == 0">
				<a :href="video.link"><img class="web-img" :src="video.image"></img></a>		
				<div class="v-left-text vd-text">
					<p class="v-title">{{video.title}}</p>
					<p class="v-subtitle">{{video.category}}<p>
					<p class="v-pgraph" v-html="video.paragraph"></p>
				</div>
			</div>
			<div class="video-right vd" v-else>
				<div class="v-right-text vd-text">
					<p class="v-title">{{video.title}}</p>
					<p class="v-subtitle">{{video.category}}<p>
					<p class="v-pgraph" v-html="video.paragraph"></p>
				</div>
				<a :href="video.link"><img class="web-img" :src="video.image"></img></a>
			</div>
			<hr v-if="vIndex != yVideos.length-1">
		</div>
	</div>
	`,
	data: function(){return{
		yVideos : [
			{link:"https://www.familyoptometricvisioncare.com", image:"/media/fovc.png", title: "Family Optometric Vision Care", paragraph:"Family Optometric Vision Care (familyoptometricvisioncare.com) is a optometrist site developed in React and React-router, originally designed in Adobe XD, managed in Wordpress (API), served on Netlify."},
			{link:"http://www.t.me/acknowledgedbot", image:"/media/acked.jpg", title:"AcknowledgedBot",category:"Bots", paragraph:"AcknowledgedBot is a Telegram Bot aimed to provide an easier and more chat-efficient way of sending and acknowledging announcements in groups. One command to announce, one command to acknowledge. <br><br>Built on Telegram Bot API with Python. Hosted on Heroku.<br><br>Try adding @AcknowledgedBot to your telegram group!"},
			{link:"/",image:"/media/homesite.jpg", title:"Personal Website",category:"Portfolio", paragraph:"This website is a single-page-app website created using VueJS Framework. Utilizing Vue-Router as well as a lightbox for graphic design page." },
			{link:"http://www.wrlowe.com/new",image:"/media/overlay.jpg", title:"Frame/Filter webapp",category:"Webapps", paragraph:"Webapp intended to take user-uploaded pictures(client-side, no server interaction) and apply pre-made filters. Utilized HTMLCanvas, made Javascript module available on Github. Created for Church audience." },
			{link:"http://www.wrlowe.com/geotester",image:"/geotester/geotester.png", title:"Geofilter Tester",category:"Webapps", paragraph:'Webapp using similar technologies as the previously created Frame/Filter webapp, taking user-uploaded Geofilters so that they may be previewed on a "Snapchat"-like video. Created for r/snapchatgeofilters subreddit.  ' },
		]
	}},
	mounted: function(){
		document.querySelector("body").style.height = 'initial';
	}
}

GalleryPage = {
	template:
	`
	<div id="videos-page">
		<h4 class="portHeader"id="photo-video">PHOTO <span style="font-size:16px">&</span> VIDEO</h4>
		<h4><a target="_blank" style="color:#1A2E4F;font-weight:300;" href="http://wenzel.pixieset.com/">Click here for Photo Gallery</a></h4>
		<div v-for="(video, vIndex) in yVideos">
			<div class="video-left vd" v-if="vIndex%2 == 0">
				<iframe width="560" height="315" :src="video.link" frameborder="0" allowfullscreen></iframe>			
				<div class="v-left-text vd-text">
					<p class="v-title">{{video.title}}</p>
					<p class="v-subtitle">{{video.category}}<p>
					<p class="v-pgraph" v-html="video.paragraph"></p>
				</div>
			</div>
			<div class="video-right vd" v-else>
				<div class="v-right-text vd-text">
					<p class="v-title">{{video.title}}</p>
					<p class="v-subtitle">{{video.category}}<p>
					<p class="v-pgraph" v-html="video.paragraph"></p>
				</div>
				<iframe width="560" height="315" :src="video.link" frameborder="0" allowfullscreen></iframe>
			</div>
			<hr v-if="vIndex != yVideos.length-1">
		</div>
	</div>

	`,
	data: function(){return{
		yVideos : [
			{link :"https://www.youtube.com/embed/videoseries?list=PLEB__KUjDwLNfXHNTU32ZfiakE0T67fa6", title:"Oakland Raiders", category: "Motion Graphics", paragraph: "Various graphics works for Oakland Raiders.<br><br>Video 1: Beyond The Cut Logo and Motion<br>Video 2: NFL Draft: The Raiders Select Logo and Motion<br>Video 3: 2018 Raiders Coaching Staff<br>Video 4: Straight From The Source Logo and Motion"},
			{link:"https://www.youtube.com/embed/yIAqXJRy6PE?list=PLEB__KUjDwLNHJ78FU15XsJrbHpiFUbjc",title:"Northwest California Cafe Night Motion Graphics (Playlist)", category:"Motion Graphics", paragraph: "Lyrical motion graphics created in Adobe After Effects."},
			{link:"https://www.youtube.com/embed/TauNi_WXfXI",title:"Summer Social Promotion",category:"Motion Graphics", paragraph:"Motion graphics video promotion intended for church socializing event optimized for Instagram and Facebook. After Effects." },
			{link:"https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FIglesiaNiCristoNewsandUpdates%2Fvideos%2F658367824348234%2F&show_text=0&width=560",title:"Northwest California 5k Recap",category:"Motion Graphics", paragraph:"Recap done for Church Event. Done in After Effects/Premiere." },			
			{link:"https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FIglesiaNiCristoNewsandUpdates%2Fvideos%2F657844774400539%2F&show_text=0&width=560",title:"Northwest California Anniversary Recap",category:"Motion Graphics", paragraph:"Recap done for Church Event. Done in After Effects/Premiere." },			
			{link:"https://www.youtube.com/embed/videoseries?list=PLEB__KUjDwLNENa4B-wwNT8fax1MrAaHo",title:"Northwest California Anniversary Promos (Playlist)",category:"Motion Graphics", paragraph:"Motion graphics (with some 3D) video created for promotion of my Church's event. Done in After Effects CC using Video Copilot's Optical Flares plugin." },
			{link:"https://www.youtube.com/embed/GA0QzucEUAU",title:"Solid As One Promo",category:"Live Action", paragraph:"Produced, directed, filmed, and edited promo for youth group event for my Church from the start. Utilized Premiere Pro for overall timeline editing, After Effects for 3D and 2D Motion Graphics. Full gear list in video description." },
			{link:"https://www.youtube.com/embed/sMJKVVwRUo4",title:"Unity Day Social Media Recap",category:"Motion Graphics", paragraph:"Produced and edited social media recap of Church event. Implemented alot of common trends in social media videos including motion graphics and style. Timelined in Premiere Pro, motion graphics in After Effects." },
			{link:"https://www.youtube.com/embed/2DJsxGlrQ-g",title:"Unity Day Promo",category:"Live Action", paragraph:"Produced, directed, filmed, and edited promotional video for Church event. Utilized Premiere Pro for overall timeline editing, After Effects for Motion Graphics. Gear list in description." },
		]
	}},
	mounted: function(){
		document.querySelector("body").style.height = 'initial';
	}
}

ResumePage = {
	template:
	`
	<div id="resume-page">
		<div id="resume-header">
			<h1>Wenzel Lowe Resume Supplemental</h1>
			<p class="resume-email">lowewenzel@gmail.com | 925-628-7700</p>
			<p class="resume-email"><a href="/LoweWenzelResume.pdf">Resume Link</a></p>
		</div>
		<div class="r-content" v-for="(project, pindex) in rProjects">
			<a style="textDecoration:none" :href="project.link" >
			<div class="r-head" :style="{backgroundColor:project.color}">
				<img :src="project.img" />
				<div class="r-section">
					<h3 class="r-title"><span style="color:#fff">{{project.title}}</span></h3>
					{{project.link}}
				</div>
			</div>
			</a>
			<div class="r-desc">
				<p>{{project.desc}}</p>
				<ul>
					<li v-for="(listitem,listindex) in project.bullets">{{listitem}}</li>
				</ul>
			</div>
		</div>
	</div>
	`,
	data: function(){return{
		rProjects: [
			{link: "http://mus.wrlowe.com/", img:"", title:"INCMusic Player Web App", color: "#c62828", desc:"INCMusic is the Christian Music department for the Iglesia Ni Cristo, filled with hundreds of original songs and compositions. The INCMusic Player is a full, reactive, music player for said songs.", bullets:["Built with React, using Redux, React-Router, material-ui, react-beautiful-dnd, wavesurfer.js, Fuse.js.","Written in ES6, JSS(SCSS)","Designed with Material Design, material-ui React components, React-transition-group", "Music player/playlist functions written from scratch using wavesurfer.js.","Previous version written in Vue, vue-router, vuex", "**http temporarily required"]},
			{link: "https://eve.wrlowe.com/", img:"", title:"INCEvents", color: "#00bcd4", desc:"INCEvents (in development) is an index of upcoming, in-progress, and past Iglesia Ni Cristo events around the world.", bullets:["Built with React, using Redux, React-Router, material-ui","Written in ES6, JSS(SCSS)","Designed with Material Design, material-ui React components, React-transition-group"]},
			{link: "http://www.t.me/actionitembot", img:"", title:"Telegram Bots @AcknowledgedBot && @ActionItemBot", color: "#444", desc:"Telegram bots for group-planning efficiency. @AcknowledgeBot makes an easier way to have team members acknowledge your message without flooding the chat. @ActionItemBot keeps track of your daily action items for your group, along with users who completed the action items.", bullets:["Built with python, telepot library","Hosted on Heroku"]},
			{link: "https://familyoptometricvisioncare.com", img:"", title:"Family Optometric Vision Care Website", color: "#124283", desc:"The Family Optometric Vision Care website is a single-page-application for reactive information on the business.", bullets:["Built with React, Wordpress API","Hosted on Netlify","Designed with custom React Bulma components"]}
		]
	}},
	mounted: function(){

	}
}

const router = new VueRouter({
	routes: [
	{ path: '/', component: Home},
	{ path: '/portfolio', component: Portfolio},
	{ path: '/portfolio/graphics', component: GraphicsGallery},
	{ path: '/portfolio/gallery', component: GalleryPage},
	{ path: '/portfolio/webdev', component: WebdevPage},
	{ path: '/portfolio/graphics/prototypes', component: PrototypesPage},
	{ path: '/portfolio/graphics/logos', component: GraphicsPage},
	{ path: '/resume', component: ResumePage},
]
})

const app = new Vue ({
	router,
	el: '#root',
	methods:{
		goBack : function(){
			router.go(-1)
		}
	}
})